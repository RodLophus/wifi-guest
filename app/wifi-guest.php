<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.png" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/wifi-guest.css" />
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wifi-guest.js"></script>
  <title>Rede sem fio - Acesso para visitantes (v.<?=$version?>)</title>
</head>

<body>
<form method="post">

  <div class="container">
  
    <div class="row row-header-1">
      <div class="col-xl-1">
        <img src="images/logo-imecc-branco.png" alt="Logotipo do IMECC" style="width:80px;height:70px" />
      </div>
      <div class="col-xl-11">
        <span class="site-title1">Instituto de Matem&aacute;tica, Estat&iacute;stica e Computa&ccedil;&atilde;o Cient&iacute;fica</span>        
      </div>
    </div>

    <div class="row row-header-2">
      <div class="col">
        <h4>Rede sem fio - Acesso para visitantes</h4>
      </div>
    </div>

    <div class="row row-banner">
      <div class="col">
        <img class="img-fluid" src="images/banner.png" />
      </div>
    </div>

    <?php if($errorMessage) { ?>
    <div class="row">
      <div class="col">
        <div class="alert alert-danger">
          <strong>ERRO:</strong> <?=$errorMessage?>
        </div>
      </div>
    </div>
    <?php } ?>
    
    <div class="row form-body">
      <div class="col">
        <h5 class="block-title">Dados do Visitante:</h5>
        <div class="form-group">
          <label for="guest-name">Nome do visitante:</label>
          <input type="text" class="form-control" id="guest-name" name="guest-name" value="<?=_vv('guest-name')?>" autofocus="autofocus" required>
        </div>
        <div class="form-group">
          <label for="guest-email">E-mail do visitante:</label>
          <input type="email" class="form-control" id="guest-email" name="guest-email" value="<?=_vv('guest-email')?>" required>
        </div>
        <div class="form-group">
          <label for="guest-period">Per&iacute;odo de perman&ecirc;ncia (dias):</label>
          <select class="custom-select" id="guest-period" name="guest-period">
            <option <?=_vs('guest-period', 'selected', '7', 'selected')?>>7</option>
            <option <?=_vs('guest-period', 'selected', '15')?>>15</option>
            <option <?=_vs('guest-period', 'selected', '30')?>>30</option>
            <option <?=_vs('guest-period', 'selected', '60')?>>60</option>
            <option <?=_vs('guest-period', 'selected', '90')?>>90</option>
            <option <?=_vs('guest-period', 'selected', '120')?>>120</option>
            <option <?=_vs('guest-period', 'selected', '180')?>>180</option>                                                                        
          </select>
        </div>
        <?php if(count($privateWifiNetworks)) { ?>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="additional-access" name="additional-access" <?=_vs('additional-access-.*', 'checked')?>>
          <label class="custom-control-label" for="additional-access">Acesso a redes sem fio adicionais</label>
        </div>
        <?php } ?>
      </div>
      <div class="col">
        <h5 class="block-title">Dados do Autorizador:</h5>
        <div class="form-group">
          <label for="authorizer-login">Nome de usu&aacute;rio (login):</label>
          <input type="text" class="form-control" autocapitalize="none" id="authorizer-login" name="authorizer-login" value="<?=_vv('authorizer-login')?>" required>
        </div>
        <div class="form-group">
          <label for="authorizer-password">Senha:</label>
          <input type="password" class="form-control" id="authorizer-password" name="authorizer-password" required>
        </div>
        <div class="form-group">
          <label for="submit-button">&nbsp;</label>
          <button type="submit" class="btn btn-primary" id="submit-button">Confirmar</button>
        </div>
      </div>
    </div>

    <div class="row footer">
      <div class="col">
        <p>IMECC - Unicamp</p>
        <p>Rua S&eacute;rgio Buarque de Holanda, 651</p>
        <p>13083-859, Campinas, SP, Brasil</p>
      </div>
      <div class="col">
        <img src="images/logo-unicamp.png" />
      </div>
    </div>

  </div

  <!-- Acessos adicionais (modal) -->
  <div class="modal fade" id="modal-additional-access" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="modal-additional-access-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-additional-access-title">Acesso a redes sem fio adicionais</h5>
        </div>
        <div class="modal-body">
          <?php foreach($privateWifiNetworks as $id => $name) { ?>
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="additional-access-<?=$id?>" name="additional-access-<?=$id?>" <?=_vs('additional-access-'.$id, 'checked')?> >
            <label class="custom-control-label" for="additional-access-<?=$id?>"><?=$name?></label>
          </div>
          <?php } ?>
          <small id="additional-access-help" class="form-text text-muted">Selecione as redes sem fio de uso restrito &agrave;s quais o visitante ter&aacute; acesso.<br />Nota: todos os visitantes ter&atilde;o acesso &agrave; rede <?=$publicWifiNetworkName?>.</small>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="modal-submit-button">Confirmar</button>
        </div>
      </div>
    </div>
  </div>

</form>
</body> 
</html>
