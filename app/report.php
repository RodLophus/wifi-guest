<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.png" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/report.css" />
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/report.js"></script>
  <title>Rede sem fio - Acesso para visitantes (v.<?=$version?>)</title>
</head>
<body>
  <div class="container">
    <div class="row row-header-1">
      <div class="col-xl-2">
        <img src="images/logo-imecc.png" alt="Logotipo do IMECC" style="width:80px;height:70px" />
      </div>
      <div class="col-xl-10">
        <h4>Instituto de Matem&aacute;tica, Estat&iacute;stica e Computa&ccedil;&atilde;o Cient&iacute;fica</h4>
      </div>
    </div>

    <div class="row row-header-2">
      <div class="col">
        <h3>Acesso à rede sem fio para visitantes</h3>
      </div>
    </div>
  
    <div class="row row-content">
      <div class="col">
        <p>Prezado(a) <b><?=_vv('guest-name')?></b>:</p>
        <p>Voc&ecirc; est&aacute; recebendo uma conta para acesso tempor&aacute;rio &agrave; rede sem fio do IMECC.</p>
        <p>Para conectar seu computador ou dispositivo m&oacute;vel &agrave; Internet, siga o seguinte procedimento:</p>
        <p><b>1.</b> <?= strpos($authorizedNetworksList, '/') ? "Acesse uma das seguintes redes sem fio: <b>$authorizedNetworksList</b>" : "Acesse a rede sem fio <b>$authorizedNetworksList</b>" ?>.</p>
        <p><b>2.</b> Aguarde alguns segundos.  O seu dispositivo deve abrir automaticamente o navegador web na p&aacute;gina do Portal de Acesso da Rede Sem Fio do IMECC.</p>
        <p><i>Obs.: se isso n&atilde;o acontecer, abra o navegador e tente acessar um site qualquer: voc&ecirc; ser&aacute; redirecionado automaticamente para o Portal de Acesso da Rede Sem Fio do IMECC (nota: acesse o site digitando o endere&ccedil;o na barra de endere&ccedil;os do navegador.  Este procedimento pode n&atilde;o funcionar se voc&ecirc; tentar acessar um site a partir da barra de buscas).</i></p>
        <p><b>3.</b> No portal da rede sem fio, clique em "Acessar a Internet" e informe os seguintes dados:</p>
        <ul>
          <li><b>Nome de usu&aacute;rio:</b> <span class="text-monospace"><mark><?=_vv('guest-email')?></mark></span></li>
          <li><b>Senha:</b> <span class="text-monospace"><mark><?=$guestPassword?></mark></span></li>
        </ul>
        <p>Pronto! O seu computador (ou dispositivo m&oacute;vel) j&aacute; est&aacute; conectado &agrave; rede sem fio do IMECC!</p>
        <p><b>Importante:</b> este acesso irá expirar em <b><?= date('d/m/Y', $guestExpirationTime) ?></b>. Se precisar continuar acessando a rede sem fio do IMECC depois dessa data, você deve solicitar o acesso novamente, junto a um professor ou responsável.</p>
        <p>Em caso de d&uacute;vidas, entre em contato com a equipe de Inform&aacute;tica do IMECC:<br />
           <b>Telefone:</b> (19) 3521-5996<br />
           <b>E-mail:</b> informatica@ime.unicamp.br<br />
           <b>Pessoalmente:</b> pr&eacute;dio principal, sala 250 (2o. andar)<br />
        </p>
      </div>
    </div>

  </div>

  <!-- Janela de aviso (modal) -->
  <div class="modal fade" id="modal-notice" tabindex="-1" role="dialog" aria-labelledby="modal-notice-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-notice-title">Acesso autorizado!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Por favor imprima as instru&ccedil;&otilde;es contidas nesta p&aacute;gina e entregue-as ao visitante.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Continuar</button>
        </div>
      </div>
    </div>
  </div>

</body>
</html>


