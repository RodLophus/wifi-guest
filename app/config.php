<?php

/**********************************************************************\
 *      Configuracao de autorizadores para as redes sem fio           *
 * A primeira entrada coresponde a rede aberta.  As demais sao redes  *
 * adicionais.  Sempre que for concedido um acesso a uma rede         *
 * adicional sera concedido tambem acesso a rede aberta.              *
 * Formato: 'SSID' => array('autorizador', '@autorizador', ...);      *
 * Onde "autorizador" pode ter o formato 'login' ou '@group' (apenas  *
 * grupos primarios sao verificados)                                  *
\**********************************************************************/
$wifiNetworks = array(
  'IMECC-ABERTA' => array('@inf', '@helpdesk', '@deprof', '@dmprof', '@dmaprof', '@secs', '@secposg', '@secgradu', '@dir')
);

/**********************************************************************\
 *                 Parametros para acesso ao LDAP                     *
\**********************************************************************/
$ldapConfig = array(
  'Server' => 'ldap.ime.unicamp.br',                        // Endereco do servidor
  'BaseDN' => 'dc=ime,dc=unicamp,dc=br',                    // Base para buscas
  'BindDN' => 'uid=wifi,ou=Proxy,dc=ime,dc=unicamp,dc=br',  // DN para logon
  'BindPassword' => 'WiFi@IMECC',                           // Senha de 'BindDN'
  'WifiGuestDN' => 'ou=WifiGuests,dc=ime,dc=unicamp,dc=br'  // DN para criar as contas de visitantes
);

?>
