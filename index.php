<?php
include('app/config.php');

$version = '1.1.3';
$errorMessage = '';
$guestPassword = '';
$privateWifiNetworks = array_slice(array_keys($wifiNetworks), 1);
$publicWifiNetworkName = array_keys($wifiNetworks)[0];

if($_POST) {
  if(pam_auth(_vv('authorizer-login'), _vv('authorizer-password'))) {
    // Autorizador autenticado
    $selectedWifiNetworks = array_merge(array($publicWifiNetworkName), getSelectedWifiNetworks());
    foreach($selectedWifiNetworks as $selectedWifiNetwork) {
      if(! checkAuthority(_vv('authorizer-login'), $selectedWifiNetwork)) {
        $errorMessage = "Autorizador sem permiss&atilde;o para conceder acesso na rede sem fio \"$selectedWifiNetwork\".";
        break;
      }
    }
  } else {
    $errorMessage = 'Autorizador com nome de usu&aacute;rio ou senha incorreta.';
  }

  if(! $errorMessage) {
    // Configura o acesso do visitante
    $guestPassword = substr(str_shuffle('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz'), 0, 10);
    $guestExpirationTime = strtotime(date('Y-m-d 23:59:59', strtotime('+'._vv('guest-period').' days')));
    $authorizedNetworksList = implode(' / ', $selectedWifiNetworks);
    if(! setWifiAccess($guestPassword, $guestExpirationTime)) {
      $errorMessage = 'N&atilde;o foi poss&iacute;vel configurar o acesso &agrave; rede sem fio.  Por favor contacte o administrador do sistema.';
    }
  }
}

if($_POST && ! $errorMessage)
  include('app/report.php');
else
  include('app/wifi-guest.php');


/**********************************************************************\
 *                             _vv()                                  *
 * Retorna o valor da variavel $_POST[$postVar], se existir           *
 **********************************************************************/
function _vv($postVar) {
  if(isset($_POST[$postVar])) {
    return $_POST[$postVar];
  }
  return '';
}


/**********************************************************************\
 *                              _vs()                                 *
 * Procura a variavel $postVarRE (por expressao regular) em $_POST[]. *
 * Retorna $valueIfOk se encontrar.                                   *
 * Se $referenceValue estiver definido, retorna $valueIfOk se a       *
 * primeira ocorrencia de $postVarRE for igual a $referenceValue.     *
 * Retorna $valueIfNotSet se $postVarRE nao for encontrada            *
\**********************************************************************/
function _vs($postVarRE, $valueIfOk, $referenceValue = null, $valueIfNotSet = '') {
  if($matches = array_values(preg_grep('/^'.$postVarRE.'$/', array_keys($_POST)))) {
    if((! $referenceValue) || ($referenceValue === $_POST[$matches[0]])) {
      return $valueIfOk;
    } else {
      return '';
    }
  }
  return $valueIfNotSet;
}


/**********************************************************************\
 *                     getSelectedWifiNetworks()                      *
 * Retorna um array com a lista de nomes das redes sem fio            *
 * selecionadas no formulario                                         *
\**********************************************************************/
function getSelectedWifiNetworks() {
  global $wifiNetworks;
  $result = array();
  foreach(preg_grep('/^additional-access-[0-9]+$/', array_keys($_POST)) as $key) {
    $i = str_replace('additional-access-', '', $key);
    $result[] = array_keys($wifiNetworks)[$i + 1];
  }
  return $result;
}


/**********************************************************************\
 *                         checkAuthority()                           *
 * Verifica se o autorizador $authorizer tem autoridade na rede sem   *
 * fio $wifiNetwork                                                   *
\**********************************************************************/
function checkAuthority($authorizer, $wifiNetwork) {
  global $wifiNetworks;
  if(isset($wifiNetworks[$wifiNetwork])) {
    // Verifica se o usuario estah autorizado a conceder acessos na rede
    if(in_array($authorizer, $wifiNetworks[$wifiNetwork])) {
      return true;
    }
    /// Verifica se o grupo do usuario estah autorizado a conceder acessos na rede
    if($authorizedGroups = preg_grep('/^@/', $wifiNetworks[$wifiNetwork])) {
      foreach(array_map(function($a) { return posix_getgrnam(substr($a, 1))['gid']; }, $authorizedGroups) as $authorizedGroupID) {
        if(posix_getpwnam($authorizer)['gid'] == $authorizedGroupID) {
          return true;
        }
      }
    }
  }
  return false;
}


/**********************************************************************\
 *                            ldapBind()                              *
 * Conecta ao servidor LDAP, utilizando os parametros do arquivo de   *
 * configuracao                                                       *
\**********************************************************************/
function ldapBind() {
  global $ldapConfig;

  if(! ($conn = ldap_connect($ldapConfig['Server']))) {
    return false;
  }
  
  ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);

  if(! ldap_bind($conn, $ldapConfig['BindDN'], $ldapConfig['BindPassword'])) {
    return false;
  }

  return $conn;
}


/**********************************************************************\
 *                            ldapGetDN()                             *
 * Efetua uma busca no LDAP e retorna o DN encontrado                 *
\**********************************************************************/
function ldapGetDN($ldapConnection, $ldapSearchDN, $ldapFilter) {
  if($result = ldap_search($ldapConnection, $ldapSearchDN, $ldapFilter)) {
    if(($entries = ldap_get_entries($ldapConnection, $result)) && $entries['count'] > 0) {
      return $entries[0]['dn'];
    }
  }
  return false;
}


/**********************************************************************\
 *                          setWifiAccess()                           *
 * Configura no LDAP o acesso do visitante a rede sem fio             *
\**********************************************************************/
function setWifiAccess($password, $expirationTime) {
  global $ldapConfig;

  if(! ($ldapConn = ldapBind())) {
    return false;
  }

  // Apaga o registro do usuario no LDAP se jah existir
  if($dn = ldapGetDN($ldapConn, $ldapConfig['WifiGuestDN'], '(cn='._vv('guest-email').')')) {
    ldap_delete($ldapConn, $dn);
  }

  $guestDN = 'cn=' . _vv('guest-email') . ',' . $ldapConfig['WifiGuestDN'];
  $authorizerDN = ldapGetDN($ldapConn, $ldapConfig['BaseDN'], '(uid='._vv('authorizer-login').')');
  $guestLastName = explode(' ', _vv('guest-name')); $guestLastName = end($guestLastName);

  // Prepara os dados a serem gravados no LDAP
  $ldapData = array(
    'objectClass' => array('person', 'organizationalPerson', 'inetOrgPerson', 'radiusprofile'),
    'mail' => _vv('guest-email'),
    'displayName' => _vv('guest-name'),
    'sn' => $guestLastName,
    'manager' => $authorizerDN, 
    'radiusTunnelPassword' => '{CRYPT}' . password_hash($password, PASSWORD_BCRYPT),
    'radiusExpiration' => $expirationTime
  );

  // Acrescenta as redes WiFi adicionais, se houver
  if($selectedWifiNetworks = getSelectedWifiNetworks()) {
    $ldapData['radiusGroupName'] = $selectedWifiNetworks;
  }

  if(! ldap_add($ldapConn, $guestDN, $ldapData)) {
    ldap_unbind($ldapConn);
    return false;
  }

  ldap_unbind($ldapConn);

  // Retorna a senha configurada para o usuario
  return $password;
}

?>
