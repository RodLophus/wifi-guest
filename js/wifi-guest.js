$(document).ready(function(){

  $('#additional-access').click(function() {
    $(this).prop('checked', !$(this).prop('checked'));
    $('#modal-additional-access').modal('show');
  });

  $('#modal-submit-button').click(function() {
    $('#modal-additional-access').modal('hide');
  });

  $('#modal-additional-access').on('hidden.bs.modal', function() {
    $('#additional-access').prop('checked', 
      $('.modal-body input[type=checkbox]:checked').length ? true : false);
  });

});
